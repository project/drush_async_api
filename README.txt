CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Indexing
 * More info
 * Configuration


INTRODUCTION
------------

Current Maintainer: Reinier Vegter <hr.vegter@gmail.com>


Drush Async API provides you a simple API (using hooks) to create queues of
operations/tasks and let them be processed multi-threaded (multi process
actually).
In the event of - for example - loading and saving all nodes in a large
content-repository, Drush Async API dramatically increases processing speed
by using a multitude of simultaneous workers, making use of all you
server's cores.

Note this module is dependent on drush and a unix/linux environment.

INSTALLATION
------------

1. Install and enable a module that makes use of this API, or enable this
module (drush_async_api) manually.


USAGE
----------------

See submodule async_node_resave to see about the API / hooks to be used.
Use drush to start 'multi-threaded' commands: `drush async resave-nodes`.

To see what operations are supporting Drush Async API: `drush async`.


MORE INFO
----------------
Workers are started by executing new drush commands from within this module.
After a worker has handled a configurable amount of items, it respawns itself
in order to make sure memory householding doesn't present a problem.
This way we won't have to rely on the php garbage collector.


CONFIGURATION
----------------
You can set variables in your settings.php to configure the performance of
Drush Async API:
 - drush_async_api_workers_num: number of simultaneous workers.
 - drush_async_api_max_batches_worker_respawn: respawn each worker
   after N batches.
 - drush_async_api_batch_size: operations in one batch.
 - drush_async_api_report_to_watchdog: report to watchdog.

See default values in drush_async_api.module.
