<?php

/**
 * @file
 * Callbacks for drush indexing.
 */

/**
 * Command callback for drush async.
 *
 * @param string $async_delta
 *   Async operation machine-name.
 *
 * @return mixed
 *   Just used for stopping code execution, no special return-data.
 */
function drush_async_api_entry($async_delta = '') {

  // Get all async-supporting modules / types.
  $async_types = module_invoke_all('drush_async_api_info');
  drupal_alter('drush_async_api_types', $async_types);

  // Track sub-processes ?
  $wait = (bool) drush_get_option('wait');
  $async_id = uniqid();

  if (empty($async_delta) || !isset($async_types[$async_delta])) {
    $list = '';
    foreach ($async_types as $delta => $info) {
      $list .= PHP_EOL . ' - ' . $delta;
    }

    drush_print(dt('Choose one of the following async operations: !ops', array('!ops' => $list)));
    return TRUE;
  }

  // Only invoke once.
  if (drush_async_api_running($async_delta)) {
    drush_print(dt('Workers already running!'));
    return FALSE;
  }

  // Get all operations to be devided by N queues.
  $async_info = $async_types[$async_delta];
  $operations = call_user_func($async_info['callback']);
  $async_info['total'] = count($operations);

  if (empty($operations)) {
    drush_print(dt('No operations to execute'));
    return TRUE;
  }

  // Set performance parameters.
  $performance_params_default = array(
    'max_workers' => DRUSH_ASYNC_API_WORKERS_NUM,
    'batch_size' => DRUSH_ASYNC_API_BATCH_SIZE,
    'respawn_num_batches' => DRUSH_ASYNC_API_MAX_BATCHES_WORKER_RESPAWN,
  );
  $async_info += $performance_params_default;

  // Initiate queues.
  // One for each concurrent process.
  $queues = array();
  for ($worker = 0; $worker < $async_info['max_workers']; $worker++) {
    $queues[$worker] = drush_async_api_get_queue($async_delta . '_' . $worker);
    // Remove and create queue.
    // Removal is to avoid endless queue-buildup if command is
    // invoked more than once.
    $queues[$worker]->deleteQueue();
    $queues[$worker]->createQueue();
  }

  // Add items to queue.
  $queue_index = 0;
  $item_queues = array();
  foreach ($operations as $operation) {
    // Append context.
    $operation['async_info'] = $async_info;

    $item_queues[$queue_index][] = $operation;
    // Cycle queues round robin.
    if ($queue_index == (count($queues) - 1)) {
      $queue_index = 0;
    }
    else {
      $queue_index++;
    }
  }
  // Create queues.
  foreach ($item_queues as $queue_index => $items) {
    $queues[$queue_index]->createItems($items);
  }

  // Spawn new process for each queue.
  // Check again is workers aren't already running.
  if (drush_async_api_running($async_delta)) {
    drush_print(dt('Workers already running!'));
    return FALSE;
  }
  foreach ($item_queues as $queue_index => $items) {
    if ($worker_total = count($items)) {
      drush_async_api_worker_invoke($async_delta, $queue_index, $async_info['respawn_num_batches'], $async_info['batch_size'], $async_info['max_workers'], $worker_total, $async_id);
    }
  }

  // User choose to wait for processes to finish.
  // Let's provide some progress feedback.
  if ($wait) {
    drush_print(dt('Waiting for sub-processes to finish.'));
    drush_print(dt('Press Ctrl-C to stop waiting. Processes will continue anyway.'));
    drush_print('..', $indent = 0, $handle = NULL, $newline = FALSE);

    $count = 0;
    while (drush_async_api_running($async_delta, $async_id)) {
      $count++;

      if ($count == 10) {
        $count = 0;

        $items_left_total = 0;
        for ($worker = 0; $worker < $async_info['max_workers']; $worker++) {
          $queue = drush_async_api_get_queue($async_delta . '_' . $worker);
          $items_left_total += $queue->numberOfItems();
        }
        $progress = round((($async_info['total'] - $items_left_total) / $async_info['total']) * 100);

        drush_print("\r" . $progress . '%', $indent = 5, $handle = NULL, $newline = FALSE);
      }
      sleep(1);
    }
  }

  return TRUE;
}

/**
 * Invoke worker-process to start processing a queue.
 *
 * @param string $async_delta
 *   Async operation machine-name.
 * @param int $worker
 *   Queue number.
 * @param int $respawn_num_batches
 *   Try to respawn process after N batches.
 * @param int $batch_size
 *   Number of items in one batch.
 * @param int $max_workers
 *   Number of worker-processes to spawn.
 *   Usually equal to number of CPU cores.
 * @param int $worker_total
 *   Total nr of operations for this worker.
 *   Used to report progress.
 * @param string $async_id
 *   ID of this particular async-invoke (equal for all workers).
 *   Used to track running-state of async-process.
 */
function drush_async_api_worker_invoke($async_delta, $worker, $respawn_num_batches, $batch_size, $max_workers, $worker_total, $async_id) {
  // Get drush binary from script runtime parameters,
  // or guess.
  $drush = escapeshellarg(variable_get('drush', 'drush'));
  if (php_sapi_name() == 'cli') {
    global $argv;
    if (strpos($argv[0], 'drush') !== FALSE) {
      $drush = escapeshellcmd($argv[0]);
    }
  }

  // Safely provide shell arguments.
  $args = implode(' ', array(
    escapeshellarg($async_delta),
    escapeshellarg($worker),
    escapeshellarg($respawn_num_batches),
    escapeshellarg($batch_size),
    escapeshellarg($max_workers),
    escapeshellarg($worker_total),
    escapeshellarg($async_id),
  ));

  $command = 'nohup ' . $drush . ' async-worker ' . $args . ' > /dev/null 2>&1 &';
  drush_print('Invoking command: ' . $command);
  exec($command);
}

/**
 * Callback for worker-process.
 *
 * Invoked by drush.
 *
 * @param string $async_delta
 *   Async operation machine-name.
 * @param int $worker
 *   Queue number.
 * @param int $respawn_num_batches
 *   Try to respawn process after N batches.
 * @param int $batch_size
 *   Number of items in one batch.
 * @param int $max_workers
 *   Number of worker-processes to spawn.
 *   Usually equal to number of CPU cores.
 * @param int $worker_total
 *   Total nr of operations for this worker.
 *   Used to report progress.
 * @param string $async_id
 *   ID of this particular async-invoke (equal for all workers).
 *   Used to track running-state of async-process.
 *
 * @return bool
 *   TRUE on success, FALSE on failure.
 */
function drush_async_api_queue_worker_entry($async_delta, $worker, $respawn_num_batches, $batch_size, $max_workers, $worker_total, $async_id) {
  if ($worker >= 0 && $worker <= $max_workers) {
    // Get queue for this worker.
    $queue = drush_async_api_get_queue($async_delta . '_' . $worker);

    // Loop queue.
    // Note that drush_async_api_respawn() might respawn this worker
    // breaking up this loop while the respawned process takes it further.
    while ($items_left = $queue->numberOfItems()) {

      // Create small batches.
      // $items will hold each batch.
      $batches = array();
      while (count($batches) < $respawn_num_batches && $operations = $queue->claimItems($batch_size)) {
        $batches[] = $operations;
        $queue->deleteItems(array_keys($operations));
      }

      // Process each batch.
      $sub_batch_counter = 0;
      foreach ($batches as $batch) {
        foreach ($batch as $operation) {
          try {
            // Callback.
            $message = call_user_func_array($operation['callback'], $operation['args']);
          }
          catch (Exception $e) {
            $message = $e->getMessage();
          }

          $sub_batch_counter++;

          // Report progress.
          $progress = round((($worker_total - $items_left + $sub_batch_counter) / $worker_total) * 100);
          watchdog('drush_async_api', 'Worker %w: %n% : %m', array(
            '%w' => $worker,
            '%n' => $progress,
            '%m' => $message,
          ), WATCHDOG_INFO);
        }
      }

      // Respawn because php GC works like crap.
      if ($queue->numberOfItems()) {
        drush_async_api_respawn();
      }
    }
  }
  return TRUE;
}

/**
 * Queue workers already spawned ?
 *
 * @param string $async_delta
 *   Async operation machine-name.
 * @param string $async_id
 *   ID of this particular async-invoke (equal for all workers).
 *   Used to track running-state of async-process.
 *
 * @return bool
 *   TRUE: yes. FALSE: no queue workers around.
 */
function drush_async_api_running($async_delta, $async_id = NULL) {
  $process_filter = "ps -ef | grep 'async-worker " . escapeshellarg($async_delta) . "\b' | grep -v grep";

  // Filter on particular async-ID.
  // Note we can't use PID's because each process respawns itself hence
  // getting a new PID each time.
  if (!is_null($async_id)) {
    $process_filter .= ' |grep ' . escapeshellarg($async_id);
  }

  exec($process_filter, $proclist);
  if (!empty($proclist)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Load queue object.
 *
 * This one provides setting, getting, claiming items in multiples.
 * Saves a HUGE amount of queries (minutes).
 *
 * @param string $name
 *   Queue name.
 *
 * @return \DrushAsyncApiQueue
 *   Queue object.
 */
function drush_async_api_get_queue($name) {
  return new DrushAsyncApiQueue($name);
}

/**
 * Respawn ME if this is a drush process.
 *
 * Used to tame memory usage vs garbage collection (lack thereof).
 */
function drush_async_api_respawn() {
  if (php_sapi_name() == 'cli') {
    global $argv;

    if (strpos($argv[0], 'drush') !== FALSE) {
      $cmd = escapeshellcmd($argv[0]);
      unset($argv[0]);
      $params = array();
      foreach ($argv as $arg) {
        $params[] = escapeshellarg($arg);
      }
      $cmd .= ' ' . implode(' ', $params);

      exec('nohup ' . $cmd . ' > /dev/null 2>&1 &');
      drupal_exit();
    }
  }
}
