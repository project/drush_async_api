<?php

/**
 * @file
 * Drush integration file.
 *
 * Drush commands:
 *  - async [async-machine-name]
 *    Multi-process handling of stuff.
 *  - async-worker [async-machine-name] [worker]
 *                 [respawn_num_batches] [batch_size] [max_workers]
 *    Worker process, don't use manually.
 */

/**
 * Implements hook_drush_command().
 */
function drush_async_api_drush_command() {

  // Include callbacks.
  include_once 'includes/drush_async_api.inc';

  $items = array();

  // Init fast indexing.
  $items['async'] = array(
    'aliases' => array(''),
    'description' => 'Index everything with use of multiple workers',
    'bootstrap' => 'DRUSH_BOOTSTRAP_DRUPAL_FULL',
    'callback' => 'drush_async_api_entry',
    'arguments' => array(
      'async-delta' => 'machine name of async operation',
    ),
    'required-arguments' => TRUE,
    'options' => array(
      'wait' => array(
        'description' => 'optional: wait untill all processes finish. Usefull for updaters etc.',
      ),
    ),
    'examples' => array(
      'drush async node-resave' => dt('Resave all nodes'),
      'drush async node-resave --wait' => dt('Resave all nodes and let drush wait for it to finish'),
    ),
  );

  // Worker drush command.
  $items['async-worker'] = array(
    'aliases' => array(''),
    'description' => 'Process stuff with use of multiple workers: queue worker (dont manually use this)',
    'bootstrap' => 'DRUSH_BOOTSTRAP_DRUPAL_FULL',
    'callback' => 'drush_async_api_queue_worker_entry',
    'arguments' => array(
      'async_delta' => 'machine name of async operation',
      'worker' => 'The worker id, 0 to n',
      'respawn_num_batches' => 'Respawn drush process after N batches',
      'batch_size' => 'Batch size to handle at once',
      'max_workers' => 'Max workers',
      'async_id' => 'ID to identify process',
    ),
    'required-arguments' => TRUE,
    'hidden' => TRUE,
  );

  return $items;
}
